Name:		newrole-gui
Version:	0.0.2
Release:	6
Summary:	ROSA newrole gui tool
Group:		Graphical desktop/Other
License:	GPLv3
Url:		https://abf.rosalinux.ru/soft/%{name}-dev
Source0:	https://abf.io/soft/%{name}-dev/archive/%{name}-dev-%{version}.tar.gz
BuildArch:	noarch

BuildRequires:	cmake
BuildRequires:	python-qt5

Requires:	python
Requires:       python-qt5
Requires:       python-pexpect
#Requires:       python-yaml
#Requires:       pyudev
#Requires:       python-dbus

%description
ROSA newrole gui tool

%prep
%setup -qn %{name}-dev-%{version}
%apply_patches

%build
bash compile-messages.sh
%cmake

%install
%makeinstall_std -C build

%files
%defattr(-,root,root)
%{_bindir}/newrole-gui
%{python_sitelib}/newrole_gui
#%{_libdir}/newrole-gui
%{_datadir}/locale/ru/LC_MESSAGES/*
