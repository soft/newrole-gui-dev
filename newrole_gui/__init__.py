__author__ = 'dal'

import gettext
import subprocess
import os
import copy
from PyQt5.QtCore import *
from PyQt5.QtWidgets import *
from PyQt5.QtGui import *
import selinux
from abc import ABCMeta, abstractmethod, abstractproperty


def getTransFunc():
    _ = None

    try:
          t = gettext.translation('newroleX_l10n', "/usr/share/locale/")
          _ = t.gettext
    except IOError:
        _ = lambda x: x

    return _

app = QApplication([])

curcontext = None
def getCurrentContext():
    global curcontext
    if curcontext is None:
        env = os.environ.copy()

        env["LANGUAGE"] = "C"
        env["LC_ALL"] = "C"
        env["LANG"] = "C"

        cmd_output = subprocess.check_output(["/bin/id", "-Z"], env = env)
        curcontext = cmd_output.split(":")

    return curcontext

class ISeinfoProvider():
    __metaclass__ = ABCMeta
    @abstractmethod
    def getRoles(self, user):
        pass
    @abstractmethod
    def getTypes(self, role):
        pass
    @abstractmethod
    def __init__(self, param):
        pass
    @abstractmethod
    def getDefaultType(self, role):
        pass

class RealSeinfoProvider(ISeinfoProvider):
    def getRoles(self, user = ""):
        if user == "" or user == None:
            user = getCurrentContext()[0]

        ret = seinfo(["-x", "-u%s" % user], '      roles:')
        ret[0].insert(0, "")
        return ret[0]
    def getTypes(self, role = ""):
        if role == "" or role == None:
            role = getCurrentContext()[1]

        ret = seinfo(["-x", "-r%s" % role], '      Types:')
        if ret:
            ret[0].insert(0, "")
        return ret[0]
    def __init__(self,param):
       pass
    def getDefaultType(self, role):
        return selinux.get_default_type(role.encode("ascii"))[1]

class ManualSeinfoProvider(ISeinfoProvider):
    def getRoles(self, user = ""):
        return self.Roles
    def getTypes(self, role = ""):
        for r in self.FullRoles:
            if r["name"]==role:
                ret = r["types"]
                ret.sort()
                return ret
        return []
    def __init__(self,param):
        self.Roles = param[0]
        self.Roles.sort()
        self.FullRoles = param[1]
    def getDefaultType(self, role):
        return ""
def seinfo(params, datatitle):
    env = os.environ.copy()

    env["LANGUAGE"] = "C"
    env["LC_ALL"] = "C"
    env["LANG"] = "C"
    try:
        cmd_output = subprocess.check_output(["/usr/bin/seinfo"] + params, env = env)
    except Exception as ex:
        return ([], str(ex))

    data = []
    data_reached = False
    for line in cmd_output.splitlines():
        if line.startswith(datatitle):
            data_reached = True
            continue

        if data_reached and line:
            data.append(line.strip())

    data.sort()
    return (data, None)

def raise_():
    raise
def validateLevel(level):
        # It's very difficult to make regex, so just try to parse
        CheckLen = lambda l: 0 if (len(l) >=1 and len(l) <= 2) else raise_(Exception())
        try:
            range = level.split("-")    # range delimiter
            CheckLen(range)
            for r in range:
                label = r.split(":")    # sensitivity/categories delimiter
                CheckLen(label)
                # try to parse sensitivity
                if label[0][0] != 's':
                    raise
                n = int(label[0][1:])
                if not (n >= 0 and n < 16):
                    raise
                if len(label) == 2:
                #try to parse categories
                    cats = label[1].split(",")
                    for cat in cats:
                # try to parse category range
                        cc = cat.split(".")
                        CheckLen(cc)
                        for c in cc:
                            if c[0] != "c":
                                raise
                            n = int(c[1:])
                            if not (n >= 0 and n < 1024):
                                raise


        except:
            return False
        return True

if __name__ == "__main__":
    # run test

    success = ["s1-s15", "s0", "s0:c0", "s0-s13:c0", "s0:c0.c1023","s0:c0.c2,c3,c6.c15"]
    fail    = ["", "wregretg", "s-1-s1023", "s0-s111111", "s1111111111", "s0-c0", "s0-cs0", "s0:c1.s3", "s0:c1.c10000", "s0:c0.c2,c3,c6.c15.c16"]

    error = False
    for s in success:
        if validateLevel(s):
            print "match: %s: should be success" % s
            error = True

    for s in fail:
        if not validateLevel(s):
            print "match: %s: should be failed" % s
            error = True

    if not error:
        print "all tests passed"