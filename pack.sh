#!/bin/bash
specname="newrole-gui.spec"
root="newrole-gui-0.0.2"
src="src"
src_tar_xz="./$root.tar.xz"

rm -rf ./$src_tar_xz
rm -rf ./$root/*
mkdir -p ./$root
mkdir ./$root/cmake
mkdir ./$root/newrole_gui
#mkdir ./$root/policy
mkdir ./$root/locale

#autoconf
cp -f CMakeLists.txt -f newrole-gui.py -f compile-messages.sh -f extract-messages.sh -f pack.sh -f newrole-gui.spec ./$root
cp ./cmake/* ./$root/cmake
cp ./newrole_gui/* ./$root/newrole_gui
#cp -R ./config/* ./$root/config
cp -R ./locale/* ./$root/locale

tar -cvJf $src_tar_xz "./$root"

rpmbuild --define="_sourcedir `pwd`"  $specname --nodeps -bs
rpmbuild --define="_sourcedir `pwd`"  $specname --ba
#rm -rf ./$root
